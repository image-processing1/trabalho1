import os
import sys
from skimage import io
import matplotlib.pyplot as plt
import numpy as np

def change_depth(img, bits):
    img = img >> 8-bits
    return img

if __name__ == '__main__':
    assert len(sys.argv)==2, "You should use image path as first argument"
    path = sys.argv[1]
    img = io.imread(path)
    print("Shape: ", img.shape, " Type: ", img.dtype)

    depth6 = change_depth(img, 6)
    depth5 = change_depth(img, 5)
    depth4 = change_depth(img, 4)
    depth3 = change_depth(img, 3)
    depth2 = change_depth(img, 2)
    depth1 = change_depth(img, 1)

    #------------PLOTING AND SAVING----------
    fig,axes = plt.subplots(nrows=2, ncols=3)
    ax = axes.ravel()

    ax[0].imshow(depth6, cmap='gray')
    ax[0].set_title("a) 64 levels")
    ax[1].imshow(depth5, cmap='gray')
    ax[1].set_title("b) 32 levels")
    ax[2].imshow(depth4, cmap='gray')
    ax[2].set_title("c) 16 levels")
    ax[3].imshow(depth3, cmap='gray')
    ax[3].set_title("d) 8 levels")
    ax[4].imshow(depth2, cmap='gray')
    ax[4].set_title("e) 4 levels")
    ax[5].imshow(depth1, cmap='gray')
    ax[5].set_title("f) 2 levels")
    ax[0].set_xlim(0, 512)
    ax[0].set_ylim(512, 0)
    plt.tight_layout()

    os.makedirs('results/',exist_ok=True)
    plt.savefig('results/ex2.png',dpi=200)
