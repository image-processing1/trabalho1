import os
import sys
from skimage import io
import matplotlib.pyplot as plt
from skimage.transform import resize
import numpy as np

def change_resolution(img, new_res):
    return resize(img, new_res)

if __name__ == '__main__':
    assert len(sys.argv)==2, "You should use image path as first argument"
    path = sys.argv[1]
    img = io.imread(path)
    print("Shape: ", img.shape, " Type: ", img.dtype)

    img128 = change_resolution(img, (128,128))
    img64 = change_resolution(img, (64,64))
    img32 = change_resolution(img, (32,32))
    img16 = change_resolution(img, (16,16))
    img8 = change_resolution(img, (8,8))

    #------------PLOTING AND SAVING----------
    fig,axes = plt.subplots(nrows=2, ncols=3)

    ax = axes.ravel()
    ax[0].imshow(img, cmap='gray')
    ax[0].set_title("a) 512x512 pixels")
    ax[1].imshow(img128, cmap='gray')
    ax[1].set_title("b) 128x128 pixels")
    ax[2].imshow(img64, cmap='gray')
    ax[2].set_title("c) 64x64 pixels")
    ax[3].imshow(img32, cmap='gray')
    ax[3].set_title("d) 32x32 pixels")
    ax[4].imshow(img16, cmap='gray')
    ax[4].set_title("e) 16x16 pixels")
    ax[5].imshow(img8, cmap='gray')
    ax[5].set_title("f) 8x8 pixels")
    ax[0].set_xlim(0, 512)
    ax[0].set_ylim(512, 0)
    plt.tight_layout()

    os.makedirs('results/',exist_ok=True)
    plt.savefig('results/ex1.png',dpi=200)
    #plt.show()
