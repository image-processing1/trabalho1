import os
import sys
from skimage import io
import matplotlib.pyplot as plt
import numpy as np

def show_plot(func='exp'):
    '''
    Given an function, show its plots
    '''
    f = []
    for x in np.linspace(0,1,100):
        f.append(x)
    f=np.asarray(f)

    if(func=='log'):
        g = np.log1p(f)
    if(func=='exp'):
        g = np.expm1(f)
    if(func=='square'):
        g = np.square(f)
    if(func=='sqrt'):
        g = np.sqrt(f)
    if(func=='contrast'):
        g = contrast_enhancement(f,alpha=0.1, beta=0.9, gamma=0.3)
    plt.plot(f,g)
    plt.xlabel('f(x)')
    plt.ylabel('g(x)')
    plt.show()

def contrast_enhancement(img, alpha=0.5, beta=0.5, gamma=0.5):
    a=0.33
    b=0.66
    f=img
    g = np.where(f<=a, alpha*f,
    np.where(f<=b, beta*(f-a)+alpha*a,
    np.where(f>b, gamma*(f-b)+beta*(b-a)+alpha*a, f)))

    return g

def transform(img, g='log', c=1):
    '''
        Given a normalized image, returns the desired transformation
    '''
    if(g == 'log'):
        return c*np.log1p(img)
    elif(g == 'exp'):
        return c*np.expm1(img)
    elif(g == 'square'):
        return c*np.square(img)
    elif(g=='sqrt'):
        return c*np.sqrt(img)

if __name__ == '__main__':
    assert len(sys.argv)==2, "You should use image path as first argument"

    path = sys.argv[1]
    img = io.imread(path)
    print("Shape: ", img.shape, " Type: ", img.dtype)
    #show_plot('contrast')

    norm_img=img/255 # changing to interval [0,1]
    logimg = transform(norm_img, 'log')*255
    expimg = transform(norm_img, 'exp')*255
    sqimg = transform(norm_img, 'square')*255
    sqrtimg = transform(norm_img, 'sqrt')*255

    contrast_img = contrast_enhancement(norm_img)

    contrast_img_aplha_small = contrast_enhancement(norm_img, alpha=0)
    contrast_img_alpha_big = contrast_enhancement(norm_img, alpha=1)

    contrast_img_beta_small = contrast_enhancement(norm_img, beta=0)
    contrast_img_beta_big = contrast_enhancement(norm_img, beta=1)

    contrast_img_gamma_small = contrast_enhancement(norm_img, gamma=0)
    contrast_img_gamma_big = contrast_enhancement(norm_img, gamma=1)

    #------------PLOTING AND SAVING----------
    #PART1
    fig,axes = plt.subplots(nrows=2, ncols=2)
    ax = axes.ravel()

    ax[0].imshow(logimg, cmap='gray')
    ax[0].set_title("a) logaritmo")
    ax[1].imshow(expimg, cmap='gray')
    ax[1].set_title("b) exponencial")
    ax[2].imshow(sqimg, cmap='gray')
    ax[2].set_title("c) quadrado")
    ax[3].imshow(sqrtimg, cmap='gray')
    ax[3].set_title("d) raiz quadrada")
    ax[0].set_xlim(0, 512)
    ax[0].set_ylim(512, 0)
    plt.tight_layout()

    os.makedirs('results/',exist_ok=True)
    plt.savefig('results/ex3-1.png',dpi=200)

    #PART2
    fig,axes = plt.subplots(nrows=2, ncols=3)
    ax = axes.ravel()
    #fig.suptitle('Valores de alpha, beta e gamma respectivamente')
    ax[0].imshow(contrast_img_aplha_small, cmap='gray')
    ax[0].set_title("a) 0.0, 0.5, 0.5")
    ax[1].imshow(contrast_img_alpha_big, cmap='gray')
    ax[1].set_title("b) 1.0, 0.5, 0.5")
    ax[2].imshow(contrast_img_beta_small, cmap='gray')
    ax[2].set_title("c) 0.5, 0.0, 0.5")
    ax[3].imshow(contrast_img_beta_big, cmap='gray')
    ax[3].set_title("d) 0.5, 1.0, 0.5")
    ax[4].imshow(contrast_img_gamma_small, cmap='gray')
    ax[4].set_title("e) 0.5, 0.5, 0.0")
    ax[5].imshow(contrast_img_gamma_big, cmap='gray')
    ax[5].set_title("f) 0.5, 0.5, 1.0")

    ax[0].set_xlim(0, 512)
    ax[0].set_ylim(512, 0)
    plt.tight_layout()

    os.makedirs('results/',exist_ok=True)
    plt.savefig('results/ex3-2.png',dpi=200)

    #plt.show()
